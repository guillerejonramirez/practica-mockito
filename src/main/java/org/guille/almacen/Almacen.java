package org.guille.almacen;

/**
 * Created by ajnebro on 9/3/16.
 */
public interface Almacen {
  boolean eliminarArticulo(String articulo, int cantidad) ;
  boolean hayArticulos(String articulo, int cantidad) ;
}
