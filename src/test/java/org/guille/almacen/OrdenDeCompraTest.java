package org.guille.almacen;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * Created by guille on 17/03/2016.
 */
//2 casos de pruba:
//uno que pruebe que si al efectuar una compra hay productos en el almacén los dos métodos de éste se han invocado
// y otro que que pruebe que si no hay suficientes productos compruebe que éstos no se han elminado del almacén.
public class OrdenDeCompraTest {
    @Test
    void pruebaMetodos(){
        Almacen almacen=mock(Almacen.class);
        OrdenDeCompra orden=new OrdenDeCompra("pipas",1);

        orden.efectuarCompra(almacen);

        verify(almacen, times(1)).eliminarArticulo("pipas",1) ;
        verify(almacen, times(1)).hayArticulos("pipas",1);
    }
    void NoProductos(){//completar mas tarde
        Almacen almacen=mock(Almacen.class);
        OrdenDeCompra orden=new OrdenDeCompra("pipas",1);

        orden.efectuarCompra(almacen);

        verify (almacen,never()).eliminarArticulo("pipas", 1);


    }
}