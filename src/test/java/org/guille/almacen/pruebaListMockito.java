package org.guille.almacen;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * Created by ajnebro on 9/3/16.
 */
public class pruebaListMockito {
  @Test
  public void pruebaLista() {
    // Creating the mock object
    List<String> list = mock(List.class) ;
      //Creating boolean to check exception
      boolean thrown=false;

    // Stubbing: defining the behavior
    when(list.get(0)).thenReturn("First") ;
    when(list.get(1)).thenReturn("Second") ;
      //apartado 1.1
      try {
          when(list.get(2)).thenThrow(new RuntimeException());
      }catch(RuntimeException e){
          thrown=true;
      }
      assertTrue(thrown);

    when(list.get(3)).thenReturn("Third");

    // Using the mock object
    System.out.println(list.get(0));
    System.out.println(list.get(1));
    System.out.println(list.get(2));

    // Verifying
    verify(list, times(1)).get(0) ;
    verify(list, times(1)).get(1);
    verify(list, times(0)).get(2);//Apartado 1.2
    verify(list, atLeastOnce()).get(3);//apartado 1.4
  }
}
